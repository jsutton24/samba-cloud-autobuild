#!/bin/bash

# create a samba domain and generate 125k users.

set -xue

./samba-domain.yml -v \
    -e ENV_NAME=g125k \
    -e primary_dc_name=g125k-dc0 \
    -e generate_users_and_groups=yes \
    -e num_users=125000 \
    -e num_max_members=125000 \
    "$@"
