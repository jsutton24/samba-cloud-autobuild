# Samba Cloud Autobuild

Samba Automation Tools.

## Install requirements

install python packages and ansible roles:

    ./ansible-env-setup.sh

It will do following things:

- install python packages in requirements.txt
- install ansible 3rd party roles in requirements.yml
- run `ansible-roles-clone-or-update.yml` to clone/update our own role repos in `~/roles` so we can debug the latest source code

NOTE: in `ansible.cfg`, we set `roles_path` to include `~/roles`, so
ansible can find roles in there.

## pview & pedit

In Catalyst, we store secrets with pview/pedit.
For this project, we need the ansible vault password, cloud credentials, etc.

Add these into your `.bashrc` or `.zshrc`:

    alias pview='ssh -t cat-prod-secret pview'
    alias pedit='ssh -t cat-prod-secret pedit'

Source `.bashrc` and then run `pview -d samba \?` to find samba secrets.

## Ansible Vault Password
With `pview`, you can find the vault password. Copy and paste it into:

    ~/.vault_password_autobuild

Password only, no extra content.

## SSH keypair

We have a common ssh keypair also store in pview. All the Linux instances
we launched in clouds will use this keypair. You must put them into:

    ~/.ssh/id_rsa_catalyst_samba
    ~/.ssh/id_rsa_catalyst_samba.pub

## OpenStack RC file

You can download your RC file from openstack dashboard.
You may need to edit `OS_PASSWORD` and remove promt for it.

source openstack rc file:

    source ~/.openstackrc

I recommand you to add this line into your `.bashrc` or `.zshrc`,
then you never forget to source it.

Test openstack CLI:

    openstack server list  # call cli tool

Test openstack dynamic inventory for ansible:

    inventory/openstack_inventory.py --list


## Rackspace RC file

Currently we setup gitlab ci private runner in Rackspace Cloud.
RackSpace doesn't provide a RC file to download like OpenStack.
But you can create one yourself with content like this:

In `~/.rackspacerc`:

    # required by rack cli
    export RS_USERNAME=<username>  # your rackspace username
    export RS_API_KEY=<api key>    # your raspace api key
    export RS_REGION_NAME=DFW

    # required by rax.py
    # ansible-role-rackspace will render this creds file
    # with $RS_USERNAME and $RS_API_KEY
    export RAX_CREDS_FILE=~/.rax_creds
    export RAX_REGION=$RS_REGION_NAME

    # helpful alias for rack cli
    alias rack-ls="rack servers instance list"
    alias rack-get="rack servers instance get --output json"
    alias rack-rm="rack servers instance delete --wait-for-completion"

Source rc file in your terminal or add it to .bashrc:

    source ~/.rackspacerc

Download `rack` executable and put it into your PATH:

    https://developer.rackspace.com/docs/rack-cli/

You may need to `chmod a+x /path/to/rack`.

To test:

    rack-ls

Since pyrax is out of maintance, in `ansible-role-rackspace`, we are using `rack` cmds.
So as long as `rack` works, this rackspace role should work.

## Setup gitlab-runner in clouds

    cd gitlab-ci/
    ./run-openstack-catalyst.sh
    ./run-rackspace-samba-team.sh

The script will call ansible role to launch instance in cloud and register to
gitlab-ci in autoscale mode. The runner will named with date, e.g.:

    gitlab-runner-openstack-0501
    gitlab-runner-rackspace-0908

Once new runners are registered, you can goto Settings -> CI/CD -> Runners to check them in your GitLab repo or group.
To replace old runners:

- pause old runners
- trigger a new pipeline or private job, check it works or not
- delete old runner instances

To debug gitlab-runner on rackspace, first list all instances:

    env | grep RS_  # ensure you sourced envvars
    rack servers instance list  # or use `rack-ls` alias if you added it

SSH to gitlab-runner:

    ssh -i ~/.ssh/id_rsa_catalyst_samba root@RUNNER-IP

NOTE: ubuntu image in rackspace only have `root` user, while catalyst cloud have `ubuntu`.

View gitlab-runner config:

    sudo vim /etc/gitlab-runner/config.toml

NOTE: gitlab-runner will change the `[[runners]]` section while you run `gitlab-runer register`.
So do not edit it directly, just view it to check.

View gitlab-runner log:

    sudo tail -f /var/log/syslog

When something went wrong, you can start a new job, and watch the log here.
Noramlly it will print the error message, like quota limit reached, docker connection problem, or wrong config.

For quota limit, you need to check both the instance quota, and keypair quota:

    rack servers instance list
    rack servers keypair list

Delete instanace or keypair:

    rack servers instance delete --name NAME
    rack servers keypair delete --name NAME

NOTE: it's quote often that we some runner instance get hanging there and
we hit the quota limit, delete the instances and don't forget the keypairs.

## View/Edit register Gitlab targets with ansible valult

To view/edit vault file, you need vault password. You can ask an existing
maintainer for that.

If you have that password in this file, then you can:

    cd gitlab-ci/
    ansible-vault view vault_catalyst.yml
    ansible-vault view vault_samba_team.yml

This file is encrypted as knowing the token could allow someone to
register fake runners for our projects.

## Create a single DC to work on it

    cd cloud-network/
    vim samba-single-dc.yml  # edit the vars, such as flavor
    ./samba-single-dc.yml -v

Then just wait about 20 mins, you will get a DC running. Login to it:

    ssh -i ~/.ssh/id_rsa_catalyst_samba ubuntu@IP


## Ansible Control Machine: sambatest1
For catalyst samba team, we use sambatest1 as our anible control machine,
to trigger cron jobs with ansible. Add these in your `~/.ssh/config`:

    Host sambatest1
        HostName cat-porwal-prod-sambatest1.wgtn.cat-it.co.nz
        User ubuntu
        IdentityFile ~/.ssh/id_rsa_catalyst_samba
        IdentitiesOnly yes

Then you can ssh to it as the ubuntu user with common keypair:

    ssh sambatest1

You can find everything in home, especially the `scripts` dir, which is a local
git repo to trace changes.

View/Edit the crontab to see how we call these scripts:

    crontab -e

To run traffic replay on a specified branch:

    ssh sambatest1
    cd scripts/
    cp run-traffic.sh run-traffic-branch.sh
    vim run-traffic-branch.sh  # edit ENV_NAME, samba_repo_url and samba_repo_version, etc.
    ./run-traffic-branch.sh

## Utils

### SSH to openstack cloud instances by name

Ensure your ssh version is 7.3+(from which the Include feature was in):

    ssh -V

Add this line at beginning of your `~/.ssh/config`:

    Include config.d/*

Generate ssh config file for all openstack instances:

    cd cloud-network
    ./openstack-ssh-config.yml -v

Check the generate config file:

    cat ~/.ssh/config.d/openstack

Login to one of your active instance:

    ssh traffic-dc0


### Destry a openstack env

A openstack env includes instance, volume, network, subnet, router, etc.
There is a util playbook to destroy a whole env:

    ./openstack-destroy-env.yml -e ENV_NAME=<ENV>


### Shelve/Unshelve a openstack env

Shelve/unshelve all instance in a env:

    ./openstack-shelve-env.yml -e ENV_NAME=<ENV>
    ./openstack-unshelve-env.yml -e ENV_NAME=<ENV>

## FAQ

### How to run traffic replay against a given branch?

SSH to sambatest1, copy `scripts/run-traffic.sh` as example, change `samba_repo_version`, setup crontab, etc.

    ssh sambatest1
    cd scripts
    cp run-traffic.sh run-traffic-branch.sh

In `run-traffic-branch.sh`, you can change/add vars:

    -e samba_repo_version=<branch> \

And use a different `ENV_NAME`:

    export ENV_NAME=<branch>

Run it directly:

    ./run-traffic-<branch>.sh

Or setup a new cron job for it:

    crontab -e  # refer to existing ones

### How to debug gitlab CI failures?

First we need to find the IP for runner bastion.

Openstack:

    openstack server instance list

Rackspace:

    rack servers instance list

Login to the machine:

Openstack:

    ssh -i ~/.ssh/id_rsa_catalyst_samba ubuntu@IP

Rackspace:

    ssh -i ~/.ssh/id_rsa_catalyst_samba root@IP

View/check config:

    sudo vim /etc/gitlab-runner/config  # check config

Ansible will generate this file first, and `gitlab-runner register` command will
change it with runners info. So avoid editing this file yourself, just check it
to ensure all options are correctly passed in to here.

Watch the log:

    sudo tail -f /var/log/syslog

If anything went wrong, normally you can spot it in log. Common issues are:

Wrong config in /etc/gitlab-runner/config, like missing username, api key, etc.
Normally this means something is wrong with your rackspacerc or ansible config.
Check that, and re-create the runner.

Instance hit quota limit. Delete the hanging instances:

    openstack server instance delete <NAME>
    rack servers instance delete --name <NAME>

Keypair hit quota limit. Delete the ghost keypairs:

    openstack server keypair delete <NAME>
    rack servers keypair delete --name <NAME>

NOTE: this one is often be ignored.
In `scripts/cloud-checker.py`, if you call it with the `--delete` option,
it will delete runner live longer than 10h and delete hanging keypairs.
We have already setup a cron job on sambatest1 to do it every workday.


## How to build a new gitlab ci docker image and how to update it?
After the bootstrap module is finished, we begin to build samba docker image
in main samba repo with gitlab ci. Refer to `.gitlab-ci.yml` and `bootstrap/`
for more details.

## How to spin up a cloud instance of 50K, 100K, bind9, CentOS?
To get a DC with large database, you can either generate the data after provision,
or restore the DC from a backup file. There are some sample playbook/script in
`cloud-network`, e.g.:

    ./samba-domain-g50k.sh  # generate 50k users
    ./samba-domain-r100k.sh  # restore from 100k backup

Please notice the above scripts actullay call `samba-domain.yml` with extra variables,
which will actually create a domain with 3 instances:
a PDC dc0, a BDC dc1, and a traffic runner.

If you only want PDC + runner, use `traffic-replay.yml` with similar variables.

If you only want single PDC, use `samba-single-dc.yml` with similar variables.

You can also adjust the image, flavor, volume size, etc.

To use a different dns backend like `BIND9_DLZ`, set var:

    samba_dns_backend=BIND9_DLZ

To restore dc from backup file instead of provision, set var:

    samba_backup_file=/path/to/local/backup/file

Refer to `traffic-replay-bind9.sh` for both example.

To setup a centos domain with bind9, refer to `samba-domain-centos-bind9.yml`.

## How to create a 4.9 DC instance and then upgrade it?

Refer to `upgrade/upgrade.yml` for example.
NOTE: to upgrade with more data, set `generate_users_and_groups=yes`.
But the generate script use `--max-members` option and ldb path as host for `scripts/traffic_replay`, which are not supported in 4.9.
You may need to adjust `ansible-role-samba-dc/templates/generate-users-and-groups.sh`.

## How to setup Windows domain?

To setup a domain with 2 windows DC, refer to `windows-domain.yml`.

To setup a domain with windows as PDC and samba as BDC, refer to `windows-pdc.yml`.

NOTE: The automation for Windows is not stable yet. The roles lie in `roles/`.

## How to use the haproxy work?

Refer to README.md at https://gitlab.com/catalyst-samba/haproxy-config
