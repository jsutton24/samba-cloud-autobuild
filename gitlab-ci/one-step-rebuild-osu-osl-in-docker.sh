#!/bin/bash -e
. $(dirname $0)/osu-osl-openrc.sh
old_umask=$(umask)
umask 077
echo $VAULT_PW > ~/.vault_password_samba_team
umask $old_umask
cd /src/
$(dirname $0)/../ansible-roles-clone-or-update.yml
$(dirname $0)/run-osu-osl-samba-team.sh "$@"
