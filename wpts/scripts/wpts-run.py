#!/usr/bin/env python
from __future__ import unicode_literals, print_function

import re
import sys
import argparse
import time

ANSI_ESCAPE = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')

from pexpect import pxssh
from pexpect.exceptions import TIMEOUT


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("hostname", help="Windows hostname")
    parser.add_argument("username", help="Windows Domain User username")
    parser.add_argument("password", help="Windows Domain User Password")

    args = parser.parse_args()

    s = pxssh.pxssh(
        echo=True,
        options={
            "StrictHostKeyChecking": "no",
            "PubkeyAuthentication": "no",
        }
    )
    s.PROMPT = '[>]'

    s.login(
        args.hostname,
        args.username,
        password=args.password,
        original_prompt='[>]',
        auto_prompt_reset=False,
    )

    s.sendline('cd C:\\')
    s.prompt()

    s.sendline('del testresult.trx')
    s.prompt()

    s.sendline('start run.cmd')
    s.prompt()

    timelapse = 0
    poll = 10
    timeout = 20 * 60
    found = False
    while timelapse <= timeout:
        time.sleep(poll)
        timelapse += poll
        print('timelapse: {}'.format(timelapse))
        # /b display bare list, no extra info
        s.sendline('cls & dir /b *.trx')
        s.prompt()
        output = ANSI_ESCAPE.sub('', s.before.decode()).strip()
        print(output, flush=True)
        if 'testresult.trx' in output:
            found = True
            break
    if found:
        print('found file, timelapse: {}'.format(timelapse))
        sys.exit(0)
    else:
        print('timeout, timelapse: {}'.format(timelapse))
        sys.exit(1)


if __name__ == '__main__':
    main()
