# Windows Protocol Test Suite automation with Ansible

## Setup Python virtualenv for Ansible

    virtualenv -p /usr/bin/python3 $HOME/ansible-env
    source $HOME/ansible-env/bin/activate
    # assume you are in current dir
    pip install -U -r ../requirements.txt

## Source your Openstack RC file

    source $HOME/sambatest.catalyst.net.nz-openrc.sh

NOTE: so far it relies on `windows-server-2012r2-x86_64-live-snapshot` in
sambatest main tenant.

## Run Ansible Playbook

To start from scratch:

    ./main.yml -v

The `main.yml` just call other playbook in this dir, you can also run
specific one directly.

E.g.: to skip provision(already done) and only run WPTS:

    ./wpts.yml -v


