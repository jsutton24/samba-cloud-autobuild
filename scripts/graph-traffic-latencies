#!/usr/bin/python
from __future__ import print_function
import matplotlib.pyplot as plt
import itertools
from hashlib import sha1, md5
import numpy as np
import os
import sys
import argparse
from collections import defaultdict, Counter
import re
from matplotlib import ticker
from pprint import pprint
from textwrap import fill
from math import log
import time

def shorten_description(s):
    s = s.lower()
    for x in ['unbind',
              'search',
              'bind',
              ]:
        if x in s:
            return x
    s = re.sub(r'^lsa_|netrlogon', '', s)
    return s

PLOTTABLE_ROWS = {'Mean': ('mean', '-.'),
                  'Median': ('median', '--'),
                  '95%': ('95th centile', ':'),
                  'Range': ('range', '-'),
                  'Max': ('maximum', '-'),
                  'Count': ('count', '-'),
                  'Failed': ('failed', '-')}
DEFAULT_ROWS = ['Median', '95%']

LINE_STYLES = (':', '--', '-.')
DOT_STYLES = ('.', 'x', '+', '|', '1', '2', '3', '4')
STYLES = LINE_STYLES


def per_sec_parser(s):
    #50259 (452.946 per second)
    m = re.search(r'(\d+) \(([\d.]+) per second\)', s)
    return {'': int(m.group(1)),
            ' per second': float(m.group(2))}


def percent_parser(x):
    return int(x[:-1])


HEADERS = {
    'Total conversations': int,
    'Successful operations': per_sec_parser,
    'Failed operations': per_sec_parser,
    'Max sleep miss': float,
    'Maximum lag': float,
    'Start lag': float,
    'Planned conversations': int,
    'Planned packets': int,
    'Unfinished conversations': int,
    'S': int,
    'r': int,
    'Percent of CPU this job got': percent_parser,
    'System time (seconds)': float,
    'User time (seconds)': float,
    'Minor (reclaiming a frame) page faults': int,
    'Maximum resident set size (kbytes)': int,
    'Major (requiring I/O) page faults': int,
    'Voluntary context switches': int,
    'Involuntary context switches': int,
    'File system outputs': int,
}


def set_bounds(ax, y_max=None):
    l, r = ax.get_xlim()
    if l < 0:
        l = 0
    l2, r2 = ax.set_xlim(left=l, right=((r + 3) * 1.1))
    ax.set_ylim(bottom=0)

    if y_max:
        b, t = ax.get_ylim()
        if t > y_max:
            ax.set_ylim(top=y_max)



def set_x_labels(ax, x_seq):
    x_seq.sort()
    x_labels = [str(x) for x in x_seq]
    skip = len(x_labels) // 20
    if skip:
        skip += 1
        x_labels = [x if (i % skip == 0) else ''
                    for i, x in enumerate(x_labels)]

    plt.xticks(x_seq, x_labels, rotation=-72, va='top',
               ha='center', fontsize='x-small')



def plot_meta_statistics(headers, pattern, log_scale=False,
                         exact_x_ticks=False,
                         variable_colour=False,
                         avoid_system_style=False,
                         split_scale=False,
                         title=None,
                         y_max=None,
                         loc=2):
    keys = set()
    # extract the r, S values from the filename
    for system in headers:
        for k, v in headers[system].items():
            m = re.search(r'r(\d+)[_-]S(\d+)', k)
            if m:
                v['r'] = int(m.group(1))
                v['S'] = int(m.group(2))
                keys.update(v.keys())

    xp, yp = pattern.split(':')
    xs = [x for x in keys if re.search(xp, x)]
    if len(xs) != 1:
        raise ValueError("%r should match a single column, not %s" %
                         (xp, xs))
    X = xs[0]
    ys = [y for y in keys if re.search(yp, y)]
    if not ys:
        raise ValueError("%r matches no columns" % yp)

    x_seq = []
    y_seqs = {}
    for system, runs in headers.items():
        for k, v in runs.items():
            xval = v.get(X)
            x_seq.append(xval)
            for y in ys:
                val = (xval, v.get(y))
                for s in headers:
                    key = ('%s (%s)' % (y, s), s)
                    if s == system:
                        y_seqs.setdefault(key, []).append(val)
                    else:
                        y_seqs.setdefault(key, []).append((xval, None))

    x_seq.sort()
    for v in y_seqs.values():
        v[:] = [x for _, x in sorted(v)]

    # plotting the surviving lines, possibly as ratios
    fig, ax = plt.subplots()

    axes = {}
    if split_scale:
        scales = []
        max_max = -1e999
        min_max = 1e999
        for k, row in y_seqs.items():
            m = log(max(x for x in row if x is not None))
            scales.append((k, m))
            min_max = min(m, min_max)
            max_max = max(m, max_max)

        if max_max > min_max + 1:
            ax2 = ax.twinx()
            for k, s in scales:
                if (max_max - s) * 2 > (s - min_max):
                   axes[k]  = ax2


    if title is None:
        title = '%s vs %s' % (X, ', '.join(ys))
    if len(title) > 119:
        title = '%s vs %d variables' % (X, len(ys))
    if len(title) > 80:
        title = fill(title, width=70)

    ax.set_title(title, fontsize=10)

    i = 0
    right = 0
    for k, row in y_seqs.items():
        _ax = axes.get(k, ax)
        legend, system = k
        legend = legend.lower().replace('per second', '/ sec')

        if _ax is not ax:
            legend += ' (right axis)'
        if avoid_system_style:
            if log_scale:
                line = _ax.semilogy(x_seq, row, '.', label=legend)
            else:
                line = _ax.plot(x_seq, row, '.', label=legend)
        else:
            c_index = i if variable_colour else 0
            colour, linestyle = get_system_style(system,
                                                 avoid_system_style,
                                                 c_index=c_index,
                                                 n_lines=len(y_seqs))
            r = plot_one_row(_ax, x_seq, row, legend, linestyle, colour, log_scale,
                             legend)
            right = max(right, r)
            i += 1

    ax.legend(fontsize=9,
              loc=loc,
              frameon=False)
    ax.set_xlabel(X)

    l, r = ax.get_xlim()
    if right > r:
        ax.set_xlim(l, right)

    set_bounds(ax, y_max)

    if exact_x_ticks:
        set_x_labels(ax, x_seq)
    tag = 'meta-stats-%s-%s' % (X, '+'.join(ys))
    show_or_write(tag)


def get_system_style(system, avoid_system_style, c_index=0, s_index=0,
                     n_lines=2):
    system = system.lower()

    if avoid_system_style or ('windows' not in system and
                              'samba' not in system):
        syshash = int(md5(system).hexdigest(), 16)
        r, g, b = [(syshash >> i) & 255 for i in (0, 8, 16)]
        dr, dg, db = [((syshash >> i) - 16) & 15
                      for i in (24, 29, 34)]
        linestyle = STYLES[s_index % len(STYLES)]
    elif 'windows' in system:
        r, g, b = 0x22, 0x66, 0xff
        dr, dg, db = 77 // n_lines, 99 // n_lines, 0 // n_lines
        linestyle = STYLES[1]
    else: # 'samba'
        r, g, b = 0xcc, 0, 0
        dr, dg, db = 30 // n_lines, 135 // n_lines, 90 // n_lines
        linestyle = STYLES[0]

    r += c_index * dr
    g += c_index * dg
    b += c_index * db
    colour = "#%02x%02x%02x" % (r & 255, g & 255, b & 255)
    print(colour, linestyle)

    return colour, linestyle


def parse_string(s):
    rows = {}
    lines = s.split('\n')
    columns = None
    headers = {}
    for line in lines:
        if columns is None:
            if line.startswith('Protocol'):
                line = line.replace('Op Code', 'Opcode')
                columns = line.split()
            else:
                line = line.strip()
                try:
                    k, v = line.rsplit(':', 1)
                except ValueError:
                    continue

                f = HEADERS.get(k)
                if f is None:
                    pass
                else:
                    fv = f(v)
                    if isinstance(fv, dict):
                        for kk, vv in fv.items():
                            headers[k + kk] = vv
                    else:
                        headers[k] = fv
            continue
        if '-->' in line:
            break
        if 'Command being timed:' in line:
            # back to headers mode
            columns = None
            continue
        values = line.split()
        row = {}
        for k, v in zip(columns, values):
            if k in ('Count', 'Failed'):
                v = int(v)
            elif k in PLOTTABLE_ROWS:
                v = float(v)
            row[k] = v

        #id = '%s:%s' % (row['Protocol'], row['Opcode'])
        #id = '%s %s' % (row['Protocol'], row['Description'])
        id = '%s %s' % (row['Protocol'], shorten_description(row['Description']))
        rows[id] = row

    # wrangle in more per-second stats
    so = headers.get('Successful operations')
    sops = headers.get('Successful operations per second')
    if sops and so:
        for k in ['Planned packets',
                  'Unfinished conversations',
                  'Planned conversations']:
            v = headers.get(k)
            if v is not None:
                headers[k + ' per second'] = v * sops / so
    # get latency extrema and typical values
    maximums = {k: 0.0 for k in PLOTTABLE_ROWS}
    weighted_sums = {k: [0.0, 0] for k in PLOTTABLE_ROWS}
    for stats in rows.values():
        for k, v in maximums.items():
            if k in stats and stats[k] > v:
                maximums[k] = stats[k]
        for k, v in weighted_sums.items():
            if k in stats and 'Count' in stats:
                v[0] += stats[k] * stats['Count']
                v[1] += stats['Count']

    for k, v in maximums.items():
        headers['latency %s' % k.lower()] = v

    for k, v in weighted_sums.items():
        if v[1]:
            headers['weighted latency %s' % k.lower()] = v[0] / v[1]

    return [headers, rows]


SAVE_NAME = None


def read_input_files(files, directory_is_system_label=False,
                     path=None, file_pattern=None):
    data = {}
    headers = {}
    all_files = []
    for fn in files:
        if path is not None and not os.path.exists(fn):
            for p in path:
                fn2 = os.path.join(p, fn)
                if os.path.exists(fn2):
                    fn = fn2
                    break
            else:
                raise IOError("%s not found in path '.', %s" %
                              (fn, ', '.join(path)))

        if os.path.isdir(fn):
            for fn2 in os.listdir(fn):
                if file_pattern is None or re.search(file_pattern, fn2):
                    all_files.append(os.path.join(fn, fn2))
        else:
            all_files.append(fn)

    #pprint(all_files)

    for fn in all_files:
        f = open(fn)
        dn, bn = os.path.split(fn)
        try:
            # old ones look like dc1_r01_S05.txt
            system, params = bn.split('_', 1)
        except ValueError as e:
            # new ones look like r01_S05.txt
            params = bn
            # the "system" is probably knowable from the full path
            # this is of course all a terrible hack
            if directory_is_system_label:
                system = re.sub(r'[^[a-zA-Z0-9.]+', ' ',
                                os.path.basename(dn)).strip().replace('stats ',
                                                                      '')
            elif 'win' in fn.lower():
                system = 'windows'
            else:
                system = 'samba'

        params = params.rsplit('.', 1)[0]
        row_headers, rows = parse_string(f.read())
        data.setdefault(system, {})[params] = rows
        headers.setdefault(system, {})[params] = row_headers
        f.close()

    return data, headers


def show_or_write(tag):
    if SAVE_NAME is None:
        plt.show()
    else:
        name = SAVE_NAME.replace('##', tag)
        fig = plt.gcf()
        fig.set_size_inches(9, 5)
        plt.tight_layout()
        plt.savefig(name, dpi=300)


def find_x_scale(fn, data):
    return int(re.search(r'S(\d\d+)', fn).group(1))


def find_x_successful_operations(fn, headers):
    return headers['Successful operations per second']


def find_x_intended_packets(fn, headers):
    return headers['Planned packets per second']


def headers_usage(headers):
    print("known headers (use substring/regex to match):")
    h = set(HEADERS)
    for x in headers.values():
        for y in x.values():
            h.update(y)
    for k in sorted(h):
        print("  %s" % k)


def plot_one_row(ax, x_seq, row, legend, linestyle, colour, log_scale, line_label):
    if set(row) == {None}:
        print("skipping empty line %s" % legend)
        return

    _x, _y = zip(*[(x, y) for x, y in zip(x_seq, row) if y is not None])
    if len(_y) == 0:
        print("missing data for %s" % legend)
        return

    if log_scale:
        line = ax.semilogy(_x, _y, linestyle, color=colour, label=legend)
    else:
        line = ax.plot(_x, _y, linestyle, color=colour, label=legend)

    bottom, top = ax.get_ylim()
    y_unit = (top - bottom) * 0.003

    chars = len(line_label)
    if chars > 50:
        line_label = fill(line_label, width=40, subsequent_indent=' ')
        chars = 40
        y_unit *= 2

    left, right = ax.get_xlim()
    x_unit = (right - left) * 0.01
    estimated_right = _x[-1] + x_unit * chars

    t = ax.text(_x[-1] + x_unit, _y[-1] - y_unit,
                line_label, color=colour,
                fontsize=9)

    return estimated_right


def parse_cmp_n(N):
    op = float.__lt__
    if N[0] in '<>':
        if N[0] == '>':
            op = float.__gt__
        N = N[1:]
    n = float(N)
    return op, n


def bad_ratio_filter(filter_string, data, headers):
    """'X:Y:[<>]n.nnn'"""
    #pprint(headers)
    #sys.exit()
    A, B, N = filter_string.split(':')
    op, n = parse_cmp_n(N)

    for system, values in headers.items():
        mask = set()
        for run, v in values.items():
            _a = v.get(A)
            _b = v.get(B)
            if _a is None or _b is None:
                #mask.add(run)
                #print('Nones:', run, _a, _b)
                continue

            a = float(_a)
            b = float(_b)
            #print(run, v, _a, _b, a, b, mask)
            if b == 0.0:
                if op == float.__lt__:
                    mask.add(run)
            elif op(a / b, n):
                mask.add(run)

        for run in values.keys():
            if run in mask:
                del values[run]
                try:
                    del data[system][run]
                except KeyError:
                    pass


def bad_value_filter(filter_string, data, headers):
    """'X[<>]n.nnn'"""
    #pprint(headers)
    #sys.exit()
    try:
        i = filter_string.index('<')
    except ValueError:
        i = filter_string.index('>')
    A = filter_string[:i]
    N = filter_string[i:]
    op, n = parse_cmp_n(N)

    for system, values in headers.items():
        mask = set()
        for run, v in values.items():
            _a = v.get(A)
            if _a is None:
                continue

            a = float(_a)
            if op(a, n):
                mask.add(run)

        for run in values.keys():
            if run in mask:
                del values[run]
                try:
                    del data[system][run]
                except KeyError:
                    pass


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output', default=None,
                        help=("write graphs here, replacing ## with type "
                              "of graph (default: show in X window)"))
    parser.add_argument('files', nargs="+",
                        help="find the data in these files")
    parser.add_argument('--path', nargs="*",
                        help="additional directory to look for file")
    parser.add_argument('--file-pattern',
                        help=("if file is directory, "
                              "filter files within with this regex"))
    parser.add_argument('-y', '--y-label', default=None,
                        help='label the y axis thusly')
    parser.add_argument('--variable-colour', action='store_true',
                        help="each pair of lines has a slightly different colour")
    parser.add_argument('-i', '--include-regex',
                        help="include protocols matching his regex")
    parser.add_argument('--min-mean-count', default=1, type=int,
                        help=("include protocols averaging at least "
                              "this many examples"))
    parser.add_argument('--ratio', action='store_true',
                        help="show the samba/windows ratio")
    parser.add_argument('--statistic',
                        help="plot this (re matching %s)" %
                        ', '.join(x.replace('%', '%%') for x in PLOTTABLE_ROWS))
    parser.add_argument('--meta-statistics',
                        help="plot these ('X:Y', regexes matching %s)" %
                        ', '.join(x.replace('%', '%%') for x in HEADERS))
    parser.add_argument('--ignore-bad-ratio', action='append',
                        help=("don't plot where ratio is above/below threshold "
                              "('X:Y:[<>]n.nnn', X, Y matching %s)" %
                              ', '.join(x.replace('%', '%%') for x in HEADERS)))
    parser.add_argument('--ignore-bad-value', action='append',
                        help=("don't plot where value is above/below threshold "
                              "('X:[<>]n.nnn', X matching %s)" %
                              ', '.join(x.replace('%', '%%') for x in HEADERS)))
    parser.add_argument('-t', '--truncate-x', type=int,
                        help='stop the x (-S) axis at this point')
    parser.add_argument('--log-scale', action='store_true',
                        help='Use a log scale for the Y axis (suits ratio)')
    parser.add_argument('-O', '--x-successful-operations', action='store_true',
                        help='X axis should be successful operations per second')
    parser.add_argument('-P', '--x-intended-packets', action='store_true',
                        help='X axis should be intended packets per second')
    parser.add_argument('--directory-is-system-label', action='store_true',
                        help='use the directory to group and label data')
    parser.add_argument('--scatter', action='store_true',
                        help='plot points not lines')
    parser.add_argument('--exact-x-ticks', action='store_true',
                        help='X scale mentions exact values')
    parser.add_argument('--y-max', type=float,
                        help='truncate y axis at this value')
    parser.add_argument('--avoid-system-style', action='store_true',
                        help='do not use consistent styles for Win/Samba')
    parser.add_argument('--split-meta-scale', action='store_true',
                        help='use different scales for meta-stats if necessary')
    parser.add_argument('--system-line-labels', action='store_true',
                        help='print system name as line labels')
    parser.add_argument('--title',
                        help='use this title instead of making one up')
    parser.add_argument('--legend-location',
                        help='put the legend here (x,y)')

    args = parser.parse_args()

    global SAVE_NAME
    SAVE_NAME = args.output

    if args.statistic:
        include_stat = re.compile(args.statistic).search
    else:
        include_stat = DEFAULT_ROWS.__contains__

    if args.x_intended_packets:
        find_x = find_x_intended_packets
        x_label = 'Attempted packets per second'
    elif args.x_successful_operations:
        find_x = find_x_successful_operations
        x_label = 'Successful operations per second'
    else:
        find_x = find_x_scale
        x_label = 'attempted relative traffic load'

    global STYLES
    if args.scatter:
        STYLES = DOT_STYLES
    else:
        STYLES = LINE_STYLES

    data, headers = read_input_files(args.files, args.directory_is_system_label,
                                     args.path, args.file_pattern)

    if args.ignore_bad_ratio:
        for x in args.ignore_bad_ratio:
            bad_ratio_filter(x, data, headers)

    if args.ignore_bad_value:
        for x in args.ignore_bad_value:
            bad_value_filter(x, data, headers)

    legend_location = args.legend_location
    if legend_location is not None:
        try:
            legend_location = int(legend_location)
        except ValueError:
            try:
                a, b = legend_location.split(',')
                legend_location = (float(a), float(b))
            except ValueError:
                pass # it should be a string like 'upper left'

    if args.meta_statistics:
        try:
            plot_meta_statistics(headers, args.meta_statistics,
                                 args.log_scale,
                                 args.exact_x_ticks,
                                 args.variable_colour,
                                 args.avoid_system_style,
                                 args.split_meta_scale,
                                 args.title,
                                 args.y_max,
                                 loc=legend_location)
        except ValueError as e:
            print(e)
            headers_usage(headers)
        return

    # At this point we have:
    #
    # data = {
    #   'win1' : {
    #      'r01_S07' : {
    #         'drsuapi:0' : {
    #             'Count': 39,
    #             'Failed': 0,
    #             'Mean': 0.14,
    #             ...
    #         },
    #         'drsuapi:1' : {
    #             ...
    #         },...
    #     },
    #      'r01_S10' : {...},
    #   },
    #   'dc1' : {...}
    # }
    #
    # We are swizzling it until we get
    #
    # x_seq = [3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 16, 19...]
    # lines = {
    #  'drsuapi:0' : {
    #      ('Windows', 'mean') : [ ...values ...],
    #      ('Windows', ' median') : [ ...values ...],
    #      ('Samba', '95%') : [ ...values ...],
    #     },


    p_totals = Counter()
    lines = {}

    all_protos = set()
    for runs in data.values():
        for run in runs.values():
            all_protos.update(run.keys())

    zero_line = defaultdict(int)

    all_x_ticks = set()
    all_x_maps = {}
    for system, runs in data.items():
        x_map = {}
        h = headers[system]
        for run_name, run in runs.items():
            if run_name not in h:
                continue
            x = find_x(run_name, h.get(run_name))
            if not args.truncate_x or args.truncate_x >= x:
                x_map[x] = run
        all_x_maps[system] = x_map
        all_x_ticks.update(x_map)

    x_seq = sorted(all_x_ticks)
    for system, x_map in all_x_maps.items():
        label = system.capitalize()
        for x in x_seq:
            run = x_map.get(x, {})

            for proto in all_protos:
                # we have to make sure we include zeros where the packet is not
                values = run.get(proto, zero_line)
                p_totals[proto] += values['Count']
                p_lines = lines.setdefault(proto, {})
                for pr in PLOTTABLE_ROWS:
                    p_lines.setdefault((label, pr), []).append(values[pr] or None)

    # filtering out unwanted lines
    n_proto = len(x_seq) * 2
    for proto in list(lines.keys()):
        total = p_totals[proto]
        if total / n_proto < args.min_mean_count:
            print("skipping %s with only %d counted over %d points" %
                  (proto, total, n_proto))
            del lines[proto]
        elif args.include_regex:
            if not re.search(args.include_regex, proto):
                print("skipping %s that doesn't match /%s/" %
                      (proto, args.include_regex))
                del lines[proto]

    # find out how many lines there will be (for colouring)
    n = 0
    for p_lines in lines.values():
        for sys, pr in p_lines.keys():
            if include_stat(pr):
                n += 1
    if args.ratio:
        n //= 2

    # plotting the surviving lines, possibly as ratios
    fig, ax = plt.subplots()

    plotted_rows = [v[0] for k, v in PLOTTABLE_ROWS.items() if include_stat(k)]

    if args.title is None:
        ax.set_title('Latency (%s)' % ', '.join(plotted_rows), fontsize=10)
    else:
        ax.set_title(args.title, fontsize=10)

    i = 0
    proto_list = sorted(lines)
    for proto in proto_list:
        p_lines = lines[proto]
        ratios = {}
        for k, row in sorted(p_lines.items()):
            system, stat = k
            if not include_stat(stat):
                continue

            if args.ratio:
                pair = ratios.setdefault(stat, [None, None])
                if system == 'Windows':
                    pair[0] = row
                else:
                    pair[1] = row
                if None in pair:
                    # this is the first pass of two
                    continue

                legend = 'ratio %s' % proto
                row = []
                i += 2
                colour = '#' + md5(proto).hexdigest()[:6]
                linestyle = PLOTTABLE_ROWS[stat][1]
                for a, b in zip(*ratios[stat]):
                    try:
                        row.append(b / a)
                    except (ZeroDivisionError, TypeError):
                        row.append(None)
            else:
                c_index = i if args.variable_colour else 0
                colour, linestyle = get_system_style(system,
                                                     args.avoid_system_style,
                                                     c_index,
                                                     proto_list.index(proto),
                                                     n)

                legend = '%s %s' % (system, proto)

            if args.system_line_labels:
                line_label = system
            else:
                line_label = proto
            # if we have more than one stat type, legend needs to include it
            if len(plotted_rows) > 1:
                legend = '%s %s' % (legend, stat.lower())
                line_label = '%s %s' % (line_label, stat.lower())

            plot_one_row(ax, x_seq, row, legend, linestyle, colour,
                         args.log_scale,
                         line_label)

            i += 1

    ax.legend(fontsize=9, loc=legend_location, frameon=False)
    ax.set_xlabel(x_label)
    if args.y_label is not None:
        ax.set_ylabel(args.y_label)
    elif args.ratio:
        ax.set_ylabel("ratio (Samba time / Windows time)")
    else:
        ax.set_ylabel("seconds (lower is better)")

    y_formatter = ticker.FormatStrFormatter('%g')
    ax.yaxis.set_major_formatter(y_formatter)

    if args.exact_x_ticks:
        set_x_labels(ax, x_seq)

    set_bounds(ax, args.y_max)

    proto_strings = [re.sub(r'\W+', '_', _) for _ in sorted(set(lines))]

    tag = 'latency-%s-%s' % ('+'.join(plotted_rows), '+'.join(proto_strings))
    for arg in ['log_scale', 'variable_colour', 'ratio', 'truncate_x']:
        if vars(args).get(arg):
            tag += '-' + arg

    tag += time.strftime('-%Y-%m-%d.%H-%M-%S')

    show_or_write(tag)


main()
